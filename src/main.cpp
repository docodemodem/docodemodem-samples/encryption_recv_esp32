/*
 * Sample program for DocodeModem
 * receive and decrypt
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <docodemo.h>
#include <SlrModem.h>
#include <AES.h>
#include <MD5.h>
#include "crc16.h"

#define SEND_PERIOD_MS 10000

const uint8_t CHANNEL = 0x10;   //10進で16チャネルです。通信相手と異なると通信できません。
const uint8_t DEVICE_DI = 0x00; //通信相手のIDです。0は全てに届きます。
const uint8_t DEVICE_EI = 0x1;  // 自分のIDです
const uint8_t DEVICE_GI = 0x02; //グループIDです。通信相手と異なると通信できません。

DOCODEMO Dm = DOCODEMO();
SlrModem modem;
HardwareSerial UartModem(MODEM_UART_NO);

//暗号化関連
AES aes;
char password[] = "Password"; //プロジェクトごとに共通にしてください。
byte aes_key[N_BLOCK];
byte aes_iv[N_BLOCK];
byte salt[8];
static MD5_CTX ctxMD5;

static int pkcs7_delete(uint8_t *src, int len)
{
  uint8_t last = src[len - 1];

  if (last <= 16)
  {
    src[len - last] = 0;
    return len - last;
  }
  else
  {
    return 0;
  }
}

void setup()
{
  Dm.begin(); //初期化が必要です。

  //デバッグ用シリアルの初期化です
  SerialDebug.begin(115200);
  while (!SerialDebug)
    ;

  //モデム用シリアルの初期化です。通信速度とポート番号を注意してください。
  UartModem.begin(MLR_BAUDRATE, SERIAL_8N1, MODEM_UART_RX_PORT, MODEM_UART_TX_PORT);
  while (!UartModem)
    ;

  //モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  //モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  //各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存するようにしてください。
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);

  //暗号化のためパスワードからkeyを作成します
  MD5::MD5Init(&ctxMD5);
  MD5::MD5Update(&ctxMD5, password, strlen(password));
  MD5::MD5Final(aes_key, &ctxMD5);
}

void loop()
{
  uint8_t rcv_buf[128 + 8];
  uint8_t buf_dec[128] = {0};

  SerialDebug.println("start loop");

  while (1)
  {
    modem.Work(); //ループ処理。受信データを処理しています。

    if (modem.HasPacket())
    {
      const uint8_t *pData;
      uint8_t rcvlen{0};

      //受信データのポインタとサイズを取得
      modem.GetPacket(&pData, &rcvlen);

      //受信データ取得
      memcpy(rcv_buf, pData, rcvlen);

      //受信データ開放
      modem.DeletePacket();

#if 0
      //改ざん又は通信エラーチェック。1ビット違っても検出できるか確認。
      int errNo = rand() % rcvlen;

      if (rcv_buf[errNo] & 0x01)
      {
        rcv_buf[errNo] &= 0xfe;
      }
      else
      {
        rcv_buf[errNo] |= 0x1;
      }
#endif

#if 1
      //受信データ表示
      for (int i = 0; i < rcvlen;i++){
        SerialDebug.printf("%02X",rcv_buf[i]);
      }
      SerialDebug.println();
#endif

      //最初の８バイトがsalt
      //復号化のため、saltとパスワードからaes_ivを作成します
      MD5::MD5Init(&ctxMD5);
      MD5::MD5Update(&ctxMD5, password, strlen(password));
      MD5::MD5Update(&ctxMD5, rcv_buf, 8);
      MD5::MD5Final(aes_iv, &ctxMD5);

      //頭のsalt(8byte)を抜いて残りを復号化
      int dec_bytes = aes.do_aes_decrypt(&rcv_buf[8], rcvlen - 8, buf_dec, aes_key, sizeof(aes_key), aes_iv);

      //補間データを削除します
      int datalen = pkcs7_delete(buf_dec, dec_bytes);

      //復号結果確認
      if (dec_bytes > 3)
      {
        //CRCチェック
        uint16_t crc_trns = buf_dec[datalen - 2] << 8 | buf_dec[datalen - 1];

        datalen -= 2; //CRC16分を減らす
        uint16_t crc_rcv = crc16(0, buf_dec, datalen);

        if (crc_trns == crc_rcv)
        {
          SerialDebug.printf("Recv Ok len=%d: ", datalen);
          SerialDebug.write(buf_dec, datalen);
        }else{
          SerialDebug.print("CRC error");
        }
      }
      SerialDebug.println();
    }

    delay(10);
  }
}